/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
import Metadata from "../planning/metadata";
import { tagParameter, tagProperty } from "./decorator_utils";
function tagged(metadataKey, metadataValue) {
    return function (target, targetKey, index) {
        var metadata = new Metadata(metadataKey, metadataValue);
        if (typeof index === "number") {
            return tagParameter(target, targetKey, index, metadata);
        }
        else {
            return tagProperty(target, targetKey, metadata);
        }
    };
}
export default tagged;
