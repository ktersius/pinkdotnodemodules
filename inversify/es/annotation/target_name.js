/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
import Metadata from "../planning/metadata";
import { tagParameter } from "./decorator_utils";
import * as METADATA_KEY from "../constants/metadata_keys";
function targetName(name) {
    return function (target, targetKey, index) {
        var metadata = new Metadata(METADATA_KEY.NAME_TAG, name);
        return tagParameter(target, targetKey, index, metadata);
    };
}
export default targetName;
