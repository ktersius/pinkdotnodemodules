/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
import BindingScope from "./binding_scope";
import BindingType from "./binding_type";
import guid from "../utils/guid";
var Binding = (function () {
    function Binding(serviceIdentifier) {
        this.guid = guid();
        this.activated = false;
        this.serviceIdentifier = serviceIdentifier;
        this.scope = BindingScope.Transient;
        this.type = BindingType.Invalid;
        this.constraint = function (request) { return true; };
        this.implementationType = null;
        this.cache = null;
        this.factory = null;
        this.provider = null;
        this.onActivation = null;
    }
    Binding.prototype.clone = function () {
        var clone = new Binding(this.serviceIdentifier);
        clone.activated = false;
        clone.implementationType = this.implementationType;
        clone.dynamicValue = this.dynamicValue;
        clone.scope = this.scope;
        clone.type = this.type;
        clone.factory = this.factory;
        clone.provider = this.provider;
        clone.constraint = this.constraint;
        clone.onActivation = this.onActivation;
        clone.cache = this.cache;
        return clone;
    };
    return Binding;
}());
export default Binding;
