/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
var BindingCount;
(function (BindingCount) {
    BindingCount[BindingCount["NoBindingsAvailable"] = 0] = "NoBindingsAvailable";
    BindingCount[BindingCount["OnlyOneBindingAvailable"] = 1] = "OnlyOneBindingAvailable";
    BindingCount[BindingCount["MultipleBindingsAvailable"] = 2] = "MultipleBindingsAvailable";
})(BindingCount || (BindingCount = {}));
export default BindingCount;
