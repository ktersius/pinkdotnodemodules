/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
var BindingScope;
(function (BindingScope) {
    BindingScope[BindingScope["Transient"] = 0] = "Transient";
    BindingScope[BindingScope["Singleton"] = 1] = "Singleton";
})(BindingScope || (BindingScope = {}));
export default BindingScope;
