/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
var BindingType;
(function (BindingType) {
    BindingType[BindingType["Invalid"] = 0] = "Invalid";
    BindingType[BindingType["Instance"] = 1] = "Instance";
    BindingType[BindingType["ConstantValue"] = 2] = "ConstantValue";
    BindingType[BindingType["DynamicValue"] = 3] = "DynamicValue";
    BindingType[BindingType["Constructor"] = 4] = "Constructor";
    BindingType[BindingType["Factory"] = 5] = "Factory";
    BindingType[BindingType["Function"] = 6] = "Function";
    BindingType[BindingType["Provider"] = 7] = "Provider";
})(BindingType || (BindingType = {}));
export default BindingType;
