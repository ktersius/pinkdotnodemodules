/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
export var DUPLICATED_INJECTABLE_DECORATOR = "Cannot apply @injectable decorator multiple times.";
export var DUPLICATED_METADATA = "Metadadata key was used more than once in a parameter:";
export var NULL_ARGUMENT = "NULL argument";
export var KEY_NOT_FOUND = "Key Not Found";
export var AMBIGUOUS_MATCH = "Ambiguous match found for serviceIdentifier:";
export var CANNOT_UNBIND = "Could not unbind serviceIdentifier:";
export var NOT_REGISTERED = "No bindings found for serviceIdentifier:";
export var MISSING_INJECTABLE_ANNOTATION = "Missing required @injectable annotation in:";
export var MISSING_INJECT_ANNOTATION = "Missing required @inject or @multiInject annotation in:";
export var CIRCULAR_DEPENDENCY = "Circular dependency found:";
export var NOT_IMPLEMENTED = "Sorry, this feature is not fully implemented yet.";
export var INVALID_BINDING_TYPE = "Invalid binding type:";
export var NO_MORE_SNAPSHOTS_AVAILABLE = "No snapshot available to restore.";
export var INVALID_MIDDLEWARE_RETURN = "Invalid return type in middleware. Return must be an Array!";
export var INVALID_FUNCTION_BINDING = "Value provided to function binding must be a function!";
export var INVALID_DECORATOR_OPERATION = "The @inject @multiInject @tagged and @named decorators " +
    "must be applied to the parameters of a class constructor or a class property.";
export var ARGUMENTS_LENGTH_MISMATCH_1 = "The number of constructor arguments in the derived class ";
export var ARGUMENTS_LENGTH_MISMATCH_2 = " must be >= than the number of constructor arguments of its base class.";
