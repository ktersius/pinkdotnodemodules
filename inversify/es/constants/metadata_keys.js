/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
export var NAMED_TAG = "named";
export var NAME_TAG = "name";
export var UNMANAGED_TAG = "unmanaged";
export var INJECT_TAG = "inject";
export var MULTI_INJECT_TAG = "multi_inject";
export var TAGGED = "inversify:tagged";
export var TAGGED_PROP = "inversify:tagged_props";
export var PARAM_TYPES = "inversify:paramtypes";
export var DESIGN_PARAM_TYPES = "design:paramtypes";
