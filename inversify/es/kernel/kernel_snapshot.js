/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
var KernelSnapshot = (function () {
    function KernelSnapshot() {
    }
    KernelSnapshot.of = function (bindings, middleware) {
        var snapshot = new KernelSnapshot();
        snapshot.bindings = bindings;
        snapshot.middleware = middleware;
        return snapshot;
    };
    return KernelSnapshot;
}());
export default KernelSnapshot;
