/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
import BindingScope from "../bindings/binding_scope";
import BindingType from "../bindings/binding_type";
import TargetType from "../planning/target_type";
import * as ERROR_MSGS from "../constants/error_msgs";
var Resolver = (function () {
    function Resolver() {
    }
    Resolver.prototype.resolve = function (context) {
        var rootRequest = context.plan.rootRequest;
        return this._resolve(rootRequest);
    };
    Resolver.prototype._resolve = function (request) {
        var _this = this;
        var bindings = request.bindings;
        var childRequests = request.childRequests;
        if (request.target && request.target.isArray() &&
            (!request.parentRequest.target || !request.parentRequest.target.matchesArray(request.target.serviceIdentifier))) {
            return childRequests.map(function (childRequest) { return _this._resolve(childRequest); });
        }
        else {
            var result = null;
            var binding = bindings[0];
            var isSingleton = binding.scope === BindingScope.Singleton;
            if (isSingleton && binding.activated === true) {
                return binding.cache;
            }
            switch (binding.type) {
                case BindingType.ConstantValue:
                    result = binding.cache;
                    break;
                case BindingType.DynamicValue:
                    result = binding.dynamicValue(request.parentContext);
                    break;
                case BindingType.Constructor:
                    result = binding.implementationType;
                    break;
                case BindingType.Factory:
                    result = binding.factory(request.parentContext);
                    break;
                case BindingType.Function:
                    result = binding.cache;
                    break;
                case BindingType.Provider:
                    result = binding.provider(request.parentContext);
                    break;
                case BindingType.Instance:
                    var constr = binding.implementationType;
                    if (childRequests.length > 0) {
                        var constructorInjectionsRequests = childRequests.filter(function (childRequest) {
                            return childRequest.target.type === TargetType.ConstructorArgument;
                        });
                        var constructorInjections = constructorInjectionsRequests.map(function (childRequest) {
                            return _this._resolve(childRequest);
                        });
                        result = this._createInstance(constr, constructorInjections);
                        result = this._injectProperties(result, childRequests);
                    }
                    else {
                        result = new constr();
                    }
                    break;
                case BindingType.Invalid:
                default:
                    var serviceIdentifier = request.parentContext.kernel.getServiceIdentifierAsString(request.serviceIdentifier);
                    throw new Error(ERROR_MSGS.INVALID_BINDING_TYPE + " " + serviceIdentifier);
            }
            if (typeof binding.onActivation === "function") {
                result = binding.onActivation(request.parentContext, result);
            }
            if (isSingleton) {
                binding.cache = result;
                binding.activated = true;
            }
            return result;
        }
    };
    Resolver.prototype._injectProperties = function (instance, childRequests) {
        var _this = this;
        var propertyInjectionsRequests = childRequests.filter(function (childRequest) {
            return childRequest.target.type === TargetType.ClassProperty;
        });
        var propertyInjections = propertyInjectionsRequests.map(function (childRequest) {
            return _this._resolve(childRequest);
        });
        propertyInjectionsRequests.forEach(function (r, index) {
            var injection = propertyInjections[index];
            instance[r.target.name.value()] = injection;
        });
        return instance;
    };
    Resolver.prototype._createInstance = function (Func, injections) {
        return new (Func.bind.apply(Func, [void 0].concat(injections)))();
    };
    return Resolver;
}());
export default Resolver;
