/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
import BindingScope from "../bindings/binding_scope";
import BindingWhenOnSyntax from "./binding_when_on_syntax";
var BindingInSyntax = (function () {
    function BindingInSyntax(binding) {
        this._binding = binding;
    }
    BindingInSyntax.prototype.inSingletonScope = function () {
        this._binding.scope = BindingScope.Singleton;
        return new BindingWhenOnSyntax(this._binding);
    };
    BindingInSyntax.prototype.inTransientScope = function () {
        this._binding.scope = BindingScope.Transient;
        return new BindingWhenOnSyntax(this._binding);
    };
    return BindingInSyntax;
}());
export default BindingInSyntax;
