/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
import BindingInWhenOnSyntax from "./binding_in_when_on_syntax";
import BindingWhenOnSyntax from "./binding_when_on_syntax";
import BindingType from "../bindings/binding_type";
import * as ERROR_MSGS from "../constants/error_msgs";
var BindingToSyntax = (function () {
    function BindingToSyntax(binding) {
        this._binding = binding;
    }
    BindingToSyntax.prototype.to = function (constructor) {
        this._binding.type = BindingType.Instance;
        this._binding.implementationType = constructor;
        return new BindingInWhenOnSyntax(this._binding);
    };
    BindingToSyntax.prototype.toSelf = function () {
        return this.to(this._binding.serviceIdentifier);
    };
    BindingToSyntax.prototype.toConstantValue = function (value) {
        this._binding.type = BindingType.ConstantValue;
        this._binding.cache = value;
        this._binding.dynamicValue = null;
        this._binding.implementationType = null;
        return new BindingWhenOnSyntax(this._binding);
    };
    BindingToSyntax.prototype.toDynamicValue = function (func) {
        this._binding.type = BindingType.DynamicValue;
        this._binding.cache = null;
        this._binding.dynamicValue = func;
        this._binding.implementationType = null;
        return new BindingWhenOnSyntax(this._binding);
    };
    BindingToSyntax.prototype.toConstructor = function (constructor) {
        this._binding.type = BindingType.Constructor;
        this._binding.implementationType = constructor;
        return new BindingWhenOnSyntax(this._binding);
    };
    BindingToSyntax.prototype.toFactory = function (factory) {
        this._binding.type = BindingType.Factory;
        this._binding.factory = factory;
        return new BindingWhenOnSyntax(this._binding);
    };
    BindingToSyntax.prototype.toFunction = function (func) {
        if (typeof func !== "function") {
            throw new Error(ERROR_MSGS.INVALID_FUNCTION_BINDING);
        }
        ;
        var bindingWhenOnSyntax = this.toConstantValue(func);
        this._binding.type = BindingType.Function;
        return bindingWhenOnSyntax;
    };
    BindingToSyntax.prototype.toAutoFactory = function (serviceIdentifier) {
        this._binding.type = BindingType.Factory;
        this._binding.factory = function (context) {
            return function () {
                return context.kernel.get(serviceIdentifier);
            };
        };
        return new BindingWhenOnSyntax(this._binding);
    };
    BindingToSyntax.prototype.toProvider = function (provider) {
        this._binding.type = BindingType.Provider;
        this._binding.provider = provider;
        return new BindingWhenOnSyntax(this._binding);
    };
    return BindingToSyntax;
}());
export default BindingToSyntax;
