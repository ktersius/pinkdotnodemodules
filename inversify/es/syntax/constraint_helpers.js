/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
import * as METADATA_KEY from "../constants/metadata_keys";
import Metadata from "../planning/metadata";
var traverseAncerstors = function (request, constraint) {
    var parent = request.parentRequest;
    if (parent !== null) {
        return constraint(parent) ? true : traverseAncerstors(parent, constraint);
    }
    else {
        return false;
    }
};
var taggedConstraint = function (key) { return function (value) {
    var constraint = function (request) {
        return request.target.matchesTag(key)(value);
    };
    constraint.metaData = new Metadata(key, value);
    return constraint;
}; };
var namedConstraint = taggedConstraint(METADATA_KEY.NAMED_TAG);
var typeConstraint = function (type) { return function (request) {
    var binding = request.bindings[0];
    if (typeof type === "string") {
        var serviceIdentifier = binding.serviceIdentifier;
        return serviceIdentifier === type;
    }
    else {
        var constructor = request.bindings[0].implementationType;
        return type === constructor;
    }
}; };
export { traverseAncerstors, taggedConstraint, namedConstraint, typeConstraint };
