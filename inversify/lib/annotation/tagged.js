/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var metadata_1 = require("../planning/metadata");
var decorator_utils_1 = require("./decorator_utils");
function tagged(metadataKey, metadataValue) {
    return function (target, targetKey, index) {
        var metadata = new metadata_1.default(metadataKey, metadataValue);
        if (typeof index === "number") {
            return decorator_utils_1.tagParameter(target, targetKey, index, metadata);
        }
        else {
            return decorator_utils_1.tagProperty(target, targetKey, metadata);
        }
    };
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = tagged;
