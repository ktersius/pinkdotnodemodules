/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var binding_scope_1 = require("./binding_scope");
var binding_type_1 = require("./binding_type");
var guid_1 = require("../utils/guid");
var Binding = (function () {
    function Binding(serviceIdentifier) {
        this.guid = guid_1.default();
        this.activated = false;
        this.serviceIdentifier = serviceIdentifier;
        this.scope = binding_scope_1.default.Transient;
        this.type = binding_type_1.default.Invalid;
        this.constraint = function (request) { return true; };
        this.implementationType = null;
        this.cache = null;
        this.factory = null;
        this.provider = null;
        this.onActivation = null;
    }
    Binding.prototype.clone = function () {
        var clone = new Binding(this.serviceIdentifier);
        clone.activated = false;
        clone.implementationType = this.implementationType;
        clone.dynamicValue = this.dynamicValue;
        clone.scope = this.scope;
        clone.type = this.type;
        clone.factory = this.factory;
        clone.provider = this.provider;
        clone.constraint = this.constraint;
        clone.onActivation = this.onActivation;
        clone.cache = this.cache;
        return clone;
    };
    return Binding;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Binding;
