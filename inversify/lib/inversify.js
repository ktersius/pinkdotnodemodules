/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var kernel_1 = require("./kernel/kernel");
exports.Kernel = kernel_1.default;
var kernel_module_1 = require("./kernel/kernel_module");
exports.KernelModule = kernel_module_1.default;
var injectable_1 = require("./annotation/injectable");
exports.injectable = injectable_1.default;
var tagged_1 = require("./annotation/tagged");
exports.tagged = tagged_1.default;
var named_1 = require("./annotation/named");
exports.named = named_1.default;
var inject_1 = require("./annotation/inject");
exports.inject = inject_1.default;
var unmanaged_1 = require("./annotation/unmanaged");
exports.unmanaged = unmanaged_1.default;
var multi_inject_1 = require("./annotation/multi_inject");
exports.multiInject = multi_inject_1.default;
var target_name_1 = require("./annotation/target_name");
exports.targetName = target_name_1.default;
var decorator_utils_1 = require("./annotation/decorator_utils");
exports.decorate = decorator_utils_1.decorate;
var constraint_helpers_1 = require("./syntax/constraint_helpers");
exports.traverseAncerstors = constraint_helpers_1.traverseAncerstors;
exports.taggedConstraint = constraint_helpers_1.taggedConstraint;
exports.namedConstraint = constraint_helpers_1.namedConstraint;
exports.typeConstraint = constraint_helpers_1.typeConstraint;
var guid_1 = require("./utils/guid");
exports.guid = guid_1.default;
