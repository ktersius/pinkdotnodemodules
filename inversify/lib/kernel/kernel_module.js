/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var guid_1 = require("../utils/guid");
var KernelModule = (function () {
    function KernelModule(registry) {
        this.guid = guid_1.default();
        this.registry = registry;
    }
    return KernelModule;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = KernelModule;
