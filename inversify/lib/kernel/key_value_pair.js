/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var guid_1 = require("../utils/guid");
var KeyValuePair = (function () {
    function KeyValuePair(serviceIdentifier, value) {
        this.serviceIdentifier = serviceIdentifier;
        this.value = new Array();
        this.value.push(value);
        this.guid = guid_1.default();
    }
    return KeyValuePair;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = KeyValuePair;
