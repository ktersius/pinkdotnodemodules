/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var key_value_pair_1 = require("./key_value_pair");
var ERROR_MSGS = require("../constants/error_msgs");
var Lookup = (function () {
    function Lookup() {
        this._dictionary = [];
    }
    Lookup.prototype.add = function (serviceIdentifier, value) {
        if (serviceIdentifier === null || serviceIdentifier === undefined) {
            throw new Error(ERROR_MSGS.NULL_ARGUMENT);
        }
        ;
        if (value === null || value === undefined) {
            throw new Error(ERROR_MSGS.NULL_ARGUMENT);
        }
        ;
        var index = this.getIndexByKey(serviceIdentifier);
        if (index !== -1) {
            this._dictionary[index].value.push(value);
        }
        else {
            this._dictionary.push(new key_value_pair_1.default(serviceIdentifier, value));
        }
    };
    Lookup.prototype.get = function (serviceIdentifier) {
        if (serviceIdentifier === null || serviceIdentifier === undefined) {
            throw new Error(ERROR_MSGS.NULL_ARGUMENT);
        }
        var index = this.getIndexByKey(serviceIdentifier);
        if (index !== -1) {
            return this._dictionary[index].value;
        }
        else {
            throw new Error(ERROR_MSGS.KEY_NOT_FOUND);
        }
    };
    Lookup.prototype.remove = function (serviceIdentifier) {
        if (serviceIdentifier === null || serviceIdentifier === undefined) {
            throw new Error(ERROR_MSGS.NULL_ARGUMENT);
        }
        var index = this.getIndexByKey(serviceIdentifier);
        if (index !== -1) {
            this._dictionary.splice(index, 1);
        }
        else {
            throw new Error(ERROR_MSGS.KEY_NOT_FOUND);
        }
    };
    Lookup.prototype.removeByModuleId = function (moduleId) {
        this._dictionary.forEach(function (keyValuePair) {
            keyValuePair.value = keyValuePair.value.filter(function (binding) {
                return binding.moduleId !== moduleId;
            });
        });
        this._dictionary = this._dictionary.filter(function (keyValuePair) {
            return keyValuePair.value.length > 0;
        });
    };
    Lookup.prototype.hasKey = function (serviceIdentifier) {
        if (serviceIdentifier === null || serviceIdentifier === undefined) {
            throw new Error(ERROR_MSGS.NULL_ARGUMENT);
        }
        var index = this.getIndexByKey(serviceIdentifier);
        if (index !== -1) {
            return true;
        }
        else {
            return false;
        }
    };
    Lookup.prototype.clone = function () {
        var l = new Lookup();
        for (var _i = 0, _a = this._dictionary; _i < _a.length; _i++) {
            var entry = _a[_i];
            for (var _b = 0, _c = entry.value; _b < _c.length; _b++) {
                var binding = _c[_b];
                l.add(entry.serviceIdentifier, binding.clone());
            }
        }
        return l;
    };
    Lookup.prototype.getIndexByKey = function (serviceIdentifier) {
        var index = -1;
        for (var i = 0; i < this._dictionary.length; i++) {
            var keyValuePair = this._dictionary[i];
            if (keyValuePair.serviceIdentifier === serviceIdentifier) {
                index = i;
            }
        }
        return index;
    };
    return Lookup;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Lookup;
