/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var guid_1 = require("../utils/guid");
var Context = (function () {
    function Context(kernel) {
        this.guid = guid_1.default();
        this.kernel = kernel;
    }
    Context.prototype.addPlan = function (plan) {
        this.plan = plan;
    };
    return Context;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Context;
