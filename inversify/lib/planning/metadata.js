/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var METADATA_KEY = require("../constants/metadata_keys");
var Metadata = (function () {
    function Metadata(key, value) {
        this.key = key;
        this.value = value;
    }
    Metadata.prototype.toString = function () {
        if (this.key === METADATA_KEY.NAMED_TAG) {
            return "named: " + this.value + " ";
        }
        else {
            return "tagged: { key:" + this.key + ", value: " + this.value + " }";
        }
    };
    return Metadata;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Metadata;
