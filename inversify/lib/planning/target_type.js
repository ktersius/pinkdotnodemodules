/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var TargetType;
(function (TargetType) {
    TargetType[TargetType["ConstructorArgument"] = 0] = "ConstructorArgument";
    TargetType[TargetType["ClassProperty"] = 1] = "ClassProperty";
})(TargetType || (TargetType = {}));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TargetType;
