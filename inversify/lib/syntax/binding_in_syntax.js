/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var binding_scope_1 = require("../bindings/binding_scope");
var binding_when_on_syntax_1 = require("./binding_when_on_syntax");
var BindingInSyntax = (function () {
    function BindingInSyntax(binding) {
        this._binding = binding;
    }
    BindingInSyntax.prototype.inSingletonScope = function () {
        this._binding.scope = binding_scope_1.default.Singleton;
        return new binding_when_on_syntax_1.default(this._binding);
    };
    BindingInSyntax.prototype.inTransientScope = function () {
        this._binding.scope = binding_scope_1.default.Transient;
        return new binding_when_on_syntax_1.default(this._binding);
    };
    return BindingInSyntax;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BindingInSyntax;
