/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
var binding_in_when_on_syntax_1 = require("./binding_in_when_on_syntax");
var binding_when_on_syntax_1 = require("./binding_when_on_syntax");
var binding_type_1 = require("../bindings/binding_type");
var ERROR_MSGS = require("../constants/error_msgs");
var BindingToSyntax = (function () {
    function BindingToSyntax(binding) {
        this._binding = binding;
    }
    BindingToSyntax.prototype.to = function (constructor) {
        this._binding.type = binding_type_1.default.Instance;
        this._binding.implementationType = constructor;
        return new binding_in_when_on_syntax_1.default(this._binding);
    };
    BindingToSyntax.prototype.toSelf = function () {
        return this.to(this._binding.serviceIdentifier);
    };
    BindingToSyntax.prototype.toConstantValue = function (value) {
        this._binding.type = binding_type_1.default.ConstantValue;
        this._binding.cache = value;
        this._binding.dynamicValue = null;
        this._binding.implementationType = null;
        return new binding_when_on_syntax_1.default(this._binding);
    };
    BindingToSyntax.prototype.toDynamicValue = function (func) {
        this._binding.type = binding_type_1.default.DynamicValue;
        this._binding.cache = null;
        this._binding.dynamicValue = func;
        this._binding.implementationType = null;
        return new binding_when_on_syntax_1.default(this._binding);
    };
    BindingToSyntax.prototype.toConstructor = function (constructor) {
        this._binding.type = binding_type_1.default.Constructor;
        this._binding.implementationType = constructor;
        return new binding_when_on_syntax_1.default(this._binding);
    };
    BindingToSyntax.prototype.toFactory = function (factory) {
        this._binding.type = binding_type_1.default.Factory;
        this._binding.factory = factory;
        return new binding_when_on_syntax_1.default(this._binding);
    };
    BindingToSyntax.prototype.toFunction = function (func) {
        if (typeof func !== "function") {
            throw new Error(ERROR_MSGS.INVALID_FUNCTION_BINDING);
        }
        ;
        var bindingWhenOnSyntax = this.toConstantValue(func);
        this._binding.type = binding_type_1.default.Function;
        return bindingWhenOnSyntax;
    };
    BindingToSyntax.prototype.toAutoFactory = function (serviceIdentifier) {
        this._binding.type = binding_type_1.default.Factory;
        this._binding.factory = function (context) {
            return function () {
                return context.kernel.get(serviceIdentifier);
            };
        };
        return new binding_when_on_syntax_1.default(this._binding);
    };
    BindingToSyntax.prototype.toProvider = function (provider) {
        this._binding.type = binding_type_1.default.Provider;
        this._binding.provider = provider;
        return new binding_when_on_syntax_1.default(this._binding);
    };
    return BindingToSyntax;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BindingToSyntax;
