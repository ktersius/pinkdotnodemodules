/**
 * inversify v.2.0.0-rc.13 - A powerful and lightweight inversion of control container for JavaScript and Node.js apps powered by TypeScript.
 * Copyright (c) 2015 Remo H. Jansen
 * MIT inversify.io/LICENSE
 * http://inversify.io
 */
"use strict";
function getFunctionName(v) {
    return v.name ? v.name : v.toString().match(/^function\s*([^\s(]+)/)[1];
}
exports.getFunctionName = getFunctionName;
