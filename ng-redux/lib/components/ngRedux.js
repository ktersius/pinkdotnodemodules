'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

exports.__esModule = true;
exports.default = ngReduxProvider;

var _connector = require('./connector');

var _connector2 = _interopRequireDefault(_connector);

var _invariant = require('invariant');

var _invariant2 = _interopRequireDefault(_invariant);

var _redux = require('redux');

var _digestMiddleware = require('./digestMiddleware');

var _digestMiddleware2 = _interopRequireDefault(_digestMiddleware);

var _lodash = require('lodash.assign');

var _lodash2 = _interopRequireDefault(_lodash);

var _lodash3 = require('lodash.isarray');

var _lodash4 = _interopRequireDefault(_lodash3);

var _lodash5 = require('lodash.isfunction');

var _lodash6 = _interopRequireDefault(_lodash5);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ngReduxProvider() {
  var _reducer = undefined;
  var _middlewares = undefined;
  var _storeEnhancers = undefined;
  var _initialState = undefined;

  this.createStoreWith = function (reducer, middlewares, storeEnhancers, initialState) {
    (0, _invariant2.default)((0, _lodash6.default)(reducer), 'The reducer parameter passed to createStoreWith must be a Function. Instead received %s.', typeof reducer === 'undefined' ? 'undefined' : _typeof(reducer));

    (0, _invariant2.default)(!storeEnhancers || (0, _lodash4.default)(storeEnhancers), 'The storeEnhancers parameter passed to createStoreWith must be an Array. Instead received %s.', typeof storeEnhancers === 'undefined' ? 'undefined' : _typeof(storeEnhancers));

    _reducer = reducer;
    _storeEnhancers = storeEnhancers;
    _middlewares = middlewares || [];
    _initialState = initialState;
  };

  this.$get = function ($injector) {
    var store = undefined,
        resolvedMiddleware = [];

    for (var _iterator = _middlewares, _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator]();;) {
      var _ref;

      if (_isArray) {
        if (_i >= _iterator.length) break;
        _ref = _iterator[_i++];
      } else {
        _i = _iterator.next();
        if (_i.done) break;
        _ref = _i.value;
      }

      var middleware = _ref;

      if (typeof middleware === 'string') {
        resolvedMiddleware.push($injector.get(middleware));
      } else {
        resolvedMiddleware.push(middleware);
      }
    }

    var finalCreateStore = _storeEnhancers ? _redux.compose.apply(undefined, _storeEnhancers)(_redux.createStore) : _redux.createStore;

    //digestMiddleware needs to be the last one.
    resolvedMiddleware.push((0, _digestMiddleware2.default)($injector.get('$rootScope')));

    store = _initialState ? _redux.applyMiddleware.apply(undefined, resolvedMiddleware)(finalCreateStore)(_reducer, _initialState) : _redux.applyMiddleware.apply(undefined, resolvedMiddleware)(finalCreateStore)(_reducer);

    return (0, _lodash2.default)({}, store, { connect: (0, _connector2.default)(store) });
  };

  this.$get.$inject = ['$injector'];
}