## v.1.1.0 - 09 Feb, 2016

Features

-  Added .exists() to check for command existence
-  Improved performance of .hasFlag()
